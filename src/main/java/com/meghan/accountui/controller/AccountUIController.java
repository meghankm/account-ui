package com.meghan.accountui.controller;

import com.meghan.accountui.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class AccountUIController {

    //interceptors will do client side load balancing
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder){
        return restTemplateBuilder.build();
    }

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.GET)
    public @ResponseBody
    Account getAccount(@PathVariable String accountId) {
        return  restTemplate.getForObject("http://config-account-service/accounts/"+accountId, Account.class);
    }
}
