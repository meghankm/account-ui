package com.meghan.accountui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class AccountUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountUiApplication.class, args);
	}

}
